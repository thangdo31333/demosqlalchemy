from flask import Flask,jsonify
import utils

app = Flask(__name__)

@app.route("/news", methods=["GET"])
def get_news():
    rows =utils.get_all("select * from news")
    data =[]
    for r in rows:
        data.append({
            "id": r[0],
            "subject": r[1],
            "description": r[2],
            "image": r[3],
            "original_url": r[4]
            
        })
    return jsonify({"news":data})

@app.route("/news/<int:news id>", methods=["GET"])
def get_news_by_id():
    pass

@app.route("/news/add",methods=["POST"])
def insert_news():
    pass

if __name__ == '__main__':
	app.run(debug = True)
