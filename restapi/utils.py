import mysql.connector

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="123456",
  database='newsdb'
)

def get_all(query):
    cur = mydb.cursor()
    cur.execute(query)
    data = cur.fetchall()
    
    return data

if __name__ == '__main__':
    print(get_all("SELECT * FROM category "))