from flask_admin import Admin
from app import app,db
from models import Category,Product,Customer
from flask_admin.contrib.sqla import ModelView

admin =Admin(app=app, name="E-commerce Administration" , template_mode= 'bootstrap4')

class ProductView(ModelView):
    can_view_details =True
    column_searchable_list = ['name','description']
    column_filters = ['name', 'price']
    
admin.add_view(ModelView(Category, db.session))
admin.add_view(ProductView(Product, db.session))
admin.add_view(ModelView(Customer, db.session))