from flask import Flask ,render_template,request,redirect,url_for

from app import app ,login
from flask_login import login_user,logout_user
import utils
import math
import cloudinary.uploader


@app.context_processor
def respone():
    return {
        'category': utils.load_categories()          
    }
    
@login.user_loader
def user_load(user_id):
    return utils.get_user_by_id(user_id=user_id)


@app.route("/")
def home():
    # get all 
   
    #by id
    cate_id = request.args.get("category_id")
    #search by name
    kw = request.args.get("keyword")
    from_price = request.args.get("from_price")
    to_price = request.args.get("to_price")
    #phan trang
    page =request.args.get("page",1)
    count_Page =utils.count_product()
    #get product
    products =utils.load_product(cate_id = cate_id,
                                kw=kw,
                                from_price=from_price,
                                to_price=to_price,page= int(page))
    return render_template("admin.html",
                           products =products,
                           pages= math.ceil(count_Page/app.config['PAGE_SIZE']))


@app.route("/product")
def product_list():
    cate_id = request.args.get("category_id")
    kw = request.args.get("keyword")
    from_price = request.args.get("from_price")
    to_price = request.args.get("to_price")
        
    pro =utils.load_product(cate_id = cate_id,
                            kw=kw,
                            from_price=from_price,
                            to_price=to_price)
    return render_template('adminProduct.html',products =pro)

@app.route("/product/<int:product_id>")
def detail(product_id):
    product =utils.get_product_by_id(product_id)
    return render_template("detail_product.html",product=product)

@app.route("/register",methods=["GET","POST"])
def user_register():
    err_msg = ""
    if request.method.__eq__("POST"):
        name =request.form.get('name')
        username =request.form.get('username')
        email =request.form.get('email')
        password =request.form.get('password')
        repass =request.form.get('repassword')
        avatar_path = None
        
        try:
            if password.strip().__eq__(repass.strip()):
                avatar = request.files.get('avatar')
                if avatar:
                    res = cloudinary.uploader.upload(avatar)
                    avatar_path=res['secure_url']
                else:
                    err_msg="Fails"    
                utils.add_user(name=name, username=username, password=password, email=email, avatar=avatar_path)
                return redirect(url_for('home'))
            else:
                err_msg="PassWord not match"
        except Exception:
            err_msg=" False" + str(Exception)

    return render_template("register.html",err_msg=err_msg)

@app.route("/login",methods=["GET","POST"])
def login():
    err_msg = ""
    if request.method.__eq__("POST"):
        username =request.form.get('username')
        password =request.form.get('password')
        user =utils.checkLogin(username=username, password=password)
        
        if user:
            login_user(user=user)
            return redirect(url_for('home'))
        else:
            err_msg="Fails"
        
    return render_template("login.html",err_msg = err_msg)
@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))
if __name__ =="__main__":
    # admin 
    from admin import *
    
    app.run(debug=True)