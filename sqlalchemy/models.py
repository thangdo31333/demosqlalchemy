from app import db
from datetime import datetime
from sqlalchemy.orm import relationship
from enum import Enum as UserEnum
from flask_login import UserMixin



class BaseModel (db.Model):
    __abstract__=True
    id =db.Column(db.Integer,primary_key =True, autoincrement =True)

class UserRole(UserEnum):
    ADMIN = 1
    USER  = 2
class Customer (BaseModel,UserMixin):
    __tablename__="customer"
    name =db.Column(db.String(100),nullable = True)
    username =db.Column(db.String(100),nullable = True,unique=True)
    password =db.Column(db.String(100),nullable =True)
    avatar =db.Column(db.String(100))
    email =db.Column(db.String(100))
    active =db.Column(db.Boolean, default =False)
    joined_date = db.Column(db.DateTime,default=datetime.now())
    user_role =db.Column(db.Enum(UserRole),default=UserRole.USER)
    
    def __str__(self):
        return self.name
        
class Category (BaseModel):
    __tablename__ ="category"
    name =db.Column(db.String(100), unique =True)
    product =relationship('Product', backref="category",lazy=True)
    
    def __str__(self):
        return self.name
    

class Product (BaseModel):
    __tablename__ ="product"
    
    name = db.Column(db.String(100),nullable =True)
    description =db.Column(db.String(100))
    price =db.Column(db.Float,default =0)
    image =db.Column(db.String(100))
    active =db.Column(db.Boolean, default =True)
    create_Date =db.Column(db.DateTime, default =datetime.now())
    category_id =db.Column(db.Integer, db.ForeignKey(Category.id), nullable =False)
    
    def __str__(self):
        return self.name
      
if __name__ =="__main__":
    db.create_all()
   
#    c1 =Category(name='Điện thoại')
#    c2 =Category(name='Máy tính')
#    c3 =Category(name='Đồng hồ')
   
   
#    db.session.add(c1)
#    db.session.add(c2)
#    db.session.add(c3)
   
#    db.session.commit()
    users =[{
                "id":1,
                "name":"thang",
                "username":"thang",
                "password":"123",
                "email":"123@123",
                "avatar":"images/bg.png"
            },
            {
                "id":2,
                "name":"tu",
                "username":"tu",
                "password":"123",
                "email":"123@123",
                "avatar":"images/bg.png"
            }]
    # products =[{
    #         "id":1,
    #         "name":"Iphone 7",
    #         "description":"sx nawm 2017",
    #         "price":1211212313,
    #         "image":"images/1.png",
    #         "category_id":1
    #     },{
    #         "id":2,
    #         "name":"Iphone 7",
    #         "description":"sx nawm 2017",
    #         "price":1211212313,
    #         "image":"images/1.png",
    #         "category_id":1
    #     },{
    #         "id":3,
    #         "name":"Iphone 7",
    #         "description":"sx nawm 2017",
    #         "price":1211212313,
    #         "image":"images/1.png",
    #         "category_id":1
    #     }]
    # for p in products:
    #    pro =Product(name=p['name'],price =p['price'],image=p['image'],description=p['description'],category_id=p['category_id'])
    #    db.session.add(pro)
    for u in users:
       user =Customer(name=u['name'],username=u['username'],password =u['password'],email=u['email'],avatar=u['avatar'])
       db.session.add(user)
    
    db.session.commit() 
       